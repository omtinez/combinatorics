import * as assert from 'assert';
import { Combinatorics } from '../combinatorics';

describe('Combinatorics', () => {
    const [a, b, c] = ['a', 'b', 'c'];

    // Necessary after upgrading to Mocha 5.2, otherwise loops forever
    after(() => setTimeout(process.exit, 1000));

    describe('#range()', () => {
        it('2^16', async () => {
            const num = Math.pow(2, 16);
            const arr = Array.of(...Combinatorics.range(num));
            assert.equal(num, arr.length);
        });

        it('zero', async () => {
            const num = 0;
            const arr = Array.of(...Combinatorics.range(num));
            assert.equal(num, arr.length);
        });
    });

    describe('#powerset()', () => {
        const mapper = <T>(arr: T[][]) =>
            arr.map(row => row.sort().join(',')).sort().join(' ');

        it('produces all possible subsets', () => {
            const expected = [[a], [a,b], [a,b,c], [a,c], [b], [b,c], [c]]
            const result = Array.of(...Combinatorics.powerset([a, b, c]));
            assert.equal(mapper(expected), mapper(result));
        });

        it('empty set', () => {
            const expected = []
            const result = Array.of(...Combinatorics.powerset([]));
            assert.equal(mapper(expected), mapper(result));
        });

        it('too long to finish', async () => {
            let counter = 0;
            const arr = Array.of(...Combinatorics.range(Math.pow(2, 16)));
            new Promise(async _ => {
                for (let _ of Combinatorics.powerset(arr))
                    await new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            });
            await new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        });
    });

    describe('#permutations()', () => {
        const mapper = <T>(arr: T[][]) =>
            arr.map(row => row.join(',')).sort().join(' ');

        it('produces all possible permutations', () => {
            const expected = [[a,b,c],[a,c,b],[b,a,c], [b,c,a], [c,a,b], [c,b,a]];
            const result = Array.of(...Combinatorics.permutations([a, b, c]));
            assert.equal(mapper(expected), mapper(result));
        });

        it('reverse sorted produces all possible permutations', () => {
            const expected = [[a,b,c],[a,c,b],[b,a,c], [b,c,a], [c,a,b], [c,b,a]];
            const result = Array.of(...Combinatorics.permutations([c, b, a]));
            assert.equal(mapper(expected), mapper(result));
        });

        it('empty set', () => {
            const expected = []
            const result = Array.of(...Combinatorics.permutations([]));
            assert.equal(mapper(expected), mapper(result));
        });

        it('too long to finish', async () => {
            let counter = 0;
            const arr = Array.of(...Combinatorics.range(Math.pow(2, 16)));
            new Promise(async _ => {
                for (let _ of Combinatorics.permutations(arr))
                    await new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            });
            await new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        });

        it('too long to finish and nested', async () => {
            let counter = 0;
            const arr = Array.of(...Combinatorics.range(Math.pow(2, 16)));
            new Promise(async _ => {
                for (let sub of Combinatorics.permutations(arr))
                    for (let _ of Combinatorics.permutations(sub))
                        await new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            });
            await new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        });
    });

    describe('#combinations()', () => {
        const mapper = <T>(arr: T[][]) =>
            arr.map(row => row.join(',')).join(' ');

        it('produces all possible combinations', () => {
            const expected = [
                [a,b,c], [a,b,3], [a,2,c], [a,2,3], [1,b,c], [1,b,3], [1,2,c], [1,2,3]];
            const result = Array.of(...Combinatorics.combinations([[a, 1], [b, 2], [c, 3]]));
            assert.equal(mapper(expected), mapper(result));
        });

        it('empty set', () => {
            const expected = []
            const result = Array.of(...Combinatorics.combinations([]));
            assert.equal(mapper(expected), mapper(result));
        });

        it('too long to finish', async () => {
            let counter = 0;
            const arr = Array.of(...Combinatorics.range(Math.pow(2, 8))).map(_ => [a, b]);;
            new Promise(async _ => {
                for (let _ of Combinatorics.combinations(arr))
                    await new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            });
            await new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        });

        it('too long to finish and nested', async () => {
            let counter = 0;
            const arr = Array.of(...Combinatorics.range(Math.pow(2, 8))).map(_ => [a, b]);;
            new Promise(async _ => {
                for (let _ of Combinatorics.combinations(arr))
                    for (let _ of Combinatorics.combinations(arr))
                        await new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            });
            await new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        });
    });

    describe('#wholeSubsets()', () => {
        const mapper = <T>(arr: T[][][]) =>
            arr.map(row => row.map(sub => sub.sort().join('+')).sort().join(',')).sort().join(' ');
            //arr.map(row => row.map(sub => sub.join('+')).join(',')).join(' ');

        it('produces all possible subsets', async () => {
            var expected = [[[a,b,c]], [[a],[b,c]], [[a,b], [c]], [[a,c],[b]], [[a],[b],[c]]];
            const result = Array.of(...Combinatorics.wholeSubsets([a, b, c]));
            assert.equal(mapper(expected), mapper(result));
        });

        it('empty set', () => {
            const expected = []
            const result = Array.of(...Combinatorics.wholeSubsets([]));
            assert.equal(mapper(expected), mapper(result));
        });

        /*
        it('too long to finish', async () => {
            let counter = 0;
            const arr = Array.of(...Combinatorics.range(Math.pow(2, 16)));
            new Promise(async _ => {
                for (let _ of Combinatorics.wholeSubsets(arr))
                    await new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            });
            await new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        });
        */
    });
});
