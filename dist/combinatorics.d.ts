export declare module Combinatorics {
    /**
     * Produces an array of integers ranging beween `[start, end)`
     * @param end last index of the array, not inclusive
     * @param start start index of the array, inclusive
     * @return array of [start..end)
     */
    function range(end: number, start?: number): IterableIterator<number>;
    /**
     * Produces all possible subsets of a given set, excluding the empty set.
     * References: https://codereview.stackexchange.com/q/7001
     * @param iter set of items
     * @return all subsets that can be drawn from original set
     */
    function powerset<T>(iter: T[]): IterableIterator<T[]>;
    /**
     * Calls back with all possible combinations when choosing one item from each set in the
     * iterable.
     * @param iterIter array-like of array-like's
     * @param callback function to be called with each result
     * @return void if callback is given, otherwise Promise<result[]>
    **/
    function combinations<T>(iterIter: T[][]): IterableIterator<T[]>;
    function permutations<T>(iter: T[]): IterableIterator<T[]>;
    /**
     * WARNING: current implementation is extremely inefficient and may not work with all objects
     * depending on behavior of sort().
     * @param iter set of items
     * @return combinations of subsets that make a whole set
    **/
    function wholeSubsets<T>(iter: T[]): IterableIterator<T[][]>;
}
