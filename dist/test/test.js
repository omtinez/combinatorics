"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const combinatorics_1 = require("../combinatorics");
describe('Combinatorics', () => {
    const [a, b, c] = ['a', 'b', 'c'];
    // Necessary after upgrading to Mocha 5.2, otherwise loops forever
    after(() => setTimeout(process.exit, 1000));
    describe('#range()', () => {
        it('2^16', () => __awaiter(this, void 0, void 0, function* () {
            const num = Math.pow(2, 16);
            const arr = Array.of(...combinatorics_1.Combinatorics.range(num));
            assert.equal(num, arr.length);
        }));
        it('zero', () => __awaiter(this, void 0, void 0, function* () {
            const num = 0;
            const arr = Array.of(...combinatorics_1.Combinatorics.range(num));
            assert.equal(num, arr.length);
        }));
    });
    describe('#powerset()', () => {
        const mapper = (arr) => arr.map(row => row.sort().join(',')).sort().join(' ');
        it('produces all possible subsets', () => {
            const expected = [[a], [a, b], [a, b, c], [a, c], [b], [b, c], [c]];
            const result = Array.of(...combinatorics_1.Combinatorics.powerset([a, b, c]));
            assert.equal(mapper(expected), mapper(result));
        });
        it('empty set', () => {
            const expected = [];
            const result = Array.of(...combinatorics_1.Combinatorics.powerset([]));
            assert.equal(mapper(expected), mapper(result));
        });
        it('too long to finish', () => __awaiter(this, void 0, void 0, function* () {
            let counter = 0;
            const arr = Array.of(...combinatorics_1.Combinatorics.range(Math.pow(2, 16)));
            new Promise((_) => __awaiter(this, void 0, void 0, function* () {
                for (let _ of combinatorics_1.Combinatorics.powerset(arr))
                    yield new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            }));
            yield new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        }));
    });
    describe('#permutations()', () => {
        const mapper = (arr) => arr.map(row => row.join(',')).sort().join(' ');
        it('produces all possible permutations', () => {
            const expected = [[a, b, c], [a, c, b], [b, a, c], [b, c, a], [c, a, b], [c, b, a]];
            const result = Array.of(...combinatorics_1.Combinatorics.permutations([a, b, c]));
            assert.equal(mapper(expected), mapper(result));
        });
        it('reverse sorted produces all possible permutations', () => {
            const expected = [[a, b, c], [a, c, b], [b, a, c], [b, c, a], [c, a, b], [c, b, a]];
            const result = Array.of(...combinatorics_1.Combinatorics.permutations([c, b, a]));
            assert.equal(mapper(expected), mapper(result));
        });
        it('empty set', () => {
            const expected = [];
            const result = Array.of(...combinatorics_1.Combinatorics.permutations([]));
            assert.equal(mapper(expected), mapper(result));
        });
        it('too long to finish', () => __awaiter(this, void 0, void 0, function* () {
            let counter = 0;
            const arr = Array.of(...combinatorics_1.Combinatorics.range(Math.pow(2, 16)));
            new Promise((_) => __awaiter(this, void 0, void 0, function* () {
                for (let _ of combinatorics_1.Combinatorics.permutations(arr))
                    yield new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            }));
            yield new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        }));
        it('too long to finish and nested', () => __awaiter(this, void 0, void 0, function* () {
            let counter = 0;
            const arr = Array.of(...combinatorics_1.Combinatorics.range(Math.pow(2, 16)));
            new Promise((_) => __awaiter(this, void 0, void 0, function* () {
                for (let sub of combinatorics_1.Combinatorics.permutations(arr))
                    for (let _ of combinatorics_1.Combinatorics.permutations(sub))
                        yield new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            }));
            yield new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        }));
    });
    describe('#combinations()', () => {
        const mapper = (arr) => arr.map(row => row.join(',')).join(' ');
        it('produces all possible combinations', () => {
            const expected = [
                [a, b, c], [a, b, 3], [a, 2, c], [a, 2, 3], [1, b, c], [1, b, 3], [1, 2, c], [1, 2, 3]
            ];
            const result = Array.of(...combinatorics_1.Combinatorics.combinations([[a, 1], [b, 2], [c, 3]]));
            assert.equal(mapper(expected), mapper(result));
        });
        it('empty set', () => {
            const expected = [];
            const result = Array.of(...combinatorics_1.Combinatorics.combinations([]));
            assert.equal(mapper(expected), mapper(result));
        });
        it('too long to finish', () => __awaiter(this, void 0, void 0, function* () {
            let counter = 0;
            const arr = Array.of(...combinatorics_1.Combinatorics.range(Math.pow(2, 8))).map(_ => [a, b]);
            ;
            new Promise((_) => __awaiter(this, void 0, void 0, function* () {
                for (let _ of combinatorics_1.Combinatorics.combinations(arr))
                    yield new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            }));
            yield new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        }));
        it('too long to finish and nested', () => __awaiter(this, void 0, void 0, function* () {
            let counter = 0;
            const arr = Array.of(...combinatorics_1.Combinatorics.range(Math.pow(2, 8))).map(_ => [a, b]);
            ;
            new Promise((_) => __awaiter(this, void 0, void 0, function* () {
                for (let _ of combinatorics_1.Combinatorics.combinations(arr))
                    for (let _ of combinatorics_1.Combinatorics.combinations(arr))
                        yield new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            }));
            yield new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        }));
    });
    describe('#wholeSubsets()', () => {
        const mapper = (arr) => arr.map(row => row.map(sub => sub.sort().join('+')).sort().join(',')).sort().join(' ');
        //arr.map(row => row.map(sub => sub.join('+')).join(',')).join(' ');
        it('produces all possible subsets', () => __awaiter(this, void 0, void 0, function* () {
            var expected = [[[a, b, c]], [[a], [b, c]], [[a, b], [c]], [[a, c], [b]], [[a], [b], [c]]];
            const result = Array.of(...combinatorics_1.Combinatorics.wholeSubsets([a, b, c]));
            assert.equal(mapper(expected), mapper(result));
        }));
        it('empty set', () => {
            const expected = [];
            const result = Array.of(...combinatorics_1.Combinatorics.wholeSubsets([]));
            assert.equal(mapper(expected), mapper(result));
        });
        /*
        it('too long to finish', async () => {
            let counter = 0;
            const arr = Array.of(...Combinatorics.range(Math.pow(2, 16)));
            new Promise(async _ => {
                for (let _ of Combinatorics.wholeSubsets(arr))
                    await new Promise((resolve, _) => setTimeout(resolve)).then(() => counter++);
                console.log(`Generator finished after ${counter} iterations`);
                counter = -1;
            });
            await new Promise((resolve, _) => setTimeout(resolve, 1000));
            assert.equal(counter > 0, true);
        });
        */
    });
});
