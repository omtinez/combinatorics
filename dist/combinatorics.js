"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Combinatorics;
(function (Combinatorics) {
    /**
     * Produces an array of integers ranging beween `[start, end)`
     * @param end last index of the array, not inclusive
     * @param start start index of the array, inclusive
     * @return array of [start..end)
     */
    function* range(end, start = 0) {
        for (let i = start; i < end; i++)
            yield i;
    }
    Combinatorics.range = range;
    /**
     * Produces all possible subsets of a given set, excluding the empty set.
     * References: https://codereview.stackexchange.com/q/7001
     * @param iter set of items
     * @return all subsets that can be drawn from original set
     */
    function* powerset(iter) {
        let subset;
        const len = iter.length;
        for (let i = 0; i < Math.pow(2, len); i++) {
            subset = [];
            for (let j = 0; j < len; j++) {
                if ((i & Math.pow(2, j))) {
                    subset.push(iter[j]);
                }
            }
            if (subset.length > 0) {
                yield subset;
            }
        }
    }
    Combinatorics.powerset = powerset;
    function _nextIterIndex(iter, ixmax) {
        // Copy indexers and compute right-most index
        const ixout = iter.slice(0);
        const ixr = iter.length - 1;
        // Base case: adding 1 to right-most index is valid
        if (++ixout[ixr] <= ixmax[ixr])
            return ixout;
        // Handle overflow case
        if (ixout.length === 1)
            throw ('Index overflow');
        // If we are over the max, we need to loop around to the next index
        return _nextIterIndex(iter.slice(0, ixr), ixmax.slice(0, ixr)).concat([0]);
    }
    /**
     * Calls back with all possible combinations when choosing one item from each set in the
     * iterable.
     * @param iterIter array-like of array-like's
     * @param callback function to be called with each result
     * @return void if callback is given, otherwise Promise<result[]>
    **/
    function* combinations(iterIter) {
        // Maximum index for each of the combination elements.
        // E.g. [[a,b], [x,y,z], [1,2]]: [2,3,2]
        const ixmax = iterIter.map(iter => iter.length - 1);
        // Keep track of the index for each sub-iter
        let ixcur = iterIter.map(_ => 0);
        // Increase ixcur until we have reached ixmax
        while (iterIter.some((_, ix) => ixcur[ix] < ixmax[ix])) {
            yield iterIter.map((iter, ix) => iter[ixcur[ix]]);
            ixcur = _nextIterIndex(ixcur, ixmax);
        }
        // Callback for the last index and return
        yield iterIter.map((iter, ix) => iter[ixcur[ix]]);
    }
    Combinatorics.combinations = combinations;
    /**
     * References: https://stackoverflow.com/a/34014930
     * @param perm array to be modified in place for the next permutation
     */
    function _permutationHelper(perm) {
        // Find non-increasing suffix
        var i = perm.length - 1;
        while (i > 0 && perm[i - 1] >= perm[i])
            i--;
        if (i <= 0)
            return false;
        // Find successor to pivot
        var j = perm.length - 1;
        while (perm[j] <= perm[i - 1])
            j--;
        var temp = perm[i - 1];
        perm[i - 1] = perm[j];
        perm[j] = temp;
        // Reverse suffix
        j = perm.length - 1;
        while (i < j) {
            temp = perm[i];
            perm[i] = perm[j];
            perm[j] = temp;
            i++;
            j--;
        }
        return true;
    }
    function* permutations(iter) {
        // Make a copy of the array since permutation will be done in-place and sort the array in
        // ascending order -- our algorithm for permutations requires it
        const perm = iter.slice(0).sort();
        // First permutation is unmodified iter
        yield perm.slice(0);
        // Iteratively return the permutations
        while (true) {
            if (!_permutationHelper(perm))
                break;
            yield perm.slice(0);
        }
    }
    Combinatorics.permutations = permutations;
    function* _subsetSplitter(iter, splitix) {
        // [a,b,c] => [1, 1]
        // [a], [b]
        let previx = 0;
        const splitnums = splitix.map((split, ix) => split ? ix : 0).filter(n => n);
        for (let currix of splitnums) {
            yield iter.slice(previx, currix);
            previx = currix;
        }
        yield iter.slice(previx, iter.length);
    }
    /**
     * WARNING: current implementation is extremely inefficient and may not work with all objects
     * depending on behavior of sort().
     * @param iter set of items
     * @return combinations of subsets that make a whole set
    **/
    function* wholeSubsets(iter) {
        const seen = {};
        for (let iter_ of permutations(iter)) {
            const indices = Array.of(...range(iter_.length));
            for (let splitix of combinations(indices.map(_ => [false, true]))) {
                const subresult = Array.of(..._subsetSplitter(iter_, splitix))
                    .map(subset => subset.sort()).sort();
                const key = JSON.stringify(subresult);
                if (!seen[key])
                    yield subresult;
                seen[key] = true;
            }
        }
    }
    Combinatorics.wholeSubsets = wholeSubsets;
})(Combinatorics = exports.Combinatorics || (exports.Combinatorics = {}));
