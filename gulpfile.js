var gulp = require('gulp');
var ts = require('gulp-typescript');
var del = require('del');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify-es').default;
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var tap = require('gulp-tap');
var pckg = require('./package.json');

// Clean tasks

gulp.task('clean', function() {
    return del('dist');
});


// Build tasks

gulp.task('ts', function() {
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            target: 'ES2015',
            module: 'CommonJS',
            declaration: true,
            noImplicitAny: true,
         }))
        .pipe(gulp.dest('dist'));
});

gulp.task('browserify', function () {
    return gulp.src('dist/' + pckg.name + '.js')
        .pipe(tap(function(file) {
            file.contents = browserify(file.path, {debug: true}).bundle();
        }))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist'));
    });

gulp.task('build', gulp.series('ts', 'browserify'));
gulp.task('default', gulp.series('build'));
